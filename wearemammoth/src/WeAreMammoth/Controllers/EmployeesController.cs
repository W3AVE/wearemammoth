﻿using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using WeAreMammoth.Models.Repositories;
using WeAreMammoth.Models;

namespace WeAreMammoth.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : Controller
    {
        [FromServices]
        public IEmployeesRepository Employees { get; set; }

        [HttpGet("home", Name = "Home Page")]
        public ActionResult Home()
        {
            return View();
        }

        [HttpGet("search/{term}", Name = "Employee Search")]
        public ActionResult Search(string term)
        {
            if(term != null)
            {
                var matchingEmployees = Employees.Search(term);
                return View(matchingEmployees);
            }
            return View();
        }
                
        public IEnumerable<Employee> GetAll()
        {
            return Employees.GetAll();
        }

    }
}
