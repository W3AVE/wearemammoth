﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeAreMammoth.Models.Repositories
{
    public class EmployeesRepository : IEmployeesRepository
    {
        static List<Employee> _employees = new List<Employee>();

        public EmployeesRepository()
        {
            Add(new Employee { Name = "Bryan Weaver", Title = "Junior Product and Support Specialist" });
            Add(new Employee { Name = "Jeremy Kratz", Title = "Software Designer/Developer" });
            Add(new Employee { Name = "Ka Wai Cheung", Title = "Author/Founder & Director of Software"});
            Add(new Employee { Name = "Ameer Mansur", Title = "System Administrator" });
            Add(new Employee { Name = "Michael Sanders", Title = "Founder & Director of Operations" });
            Add(new Employee { Name = "Craig Bryant", Title = "Founder & Director of Products" });
            Add(new Employee { Name = "Scott Ladue", Title = "Project Manager" });
            Add(new Employee { Name = "Jason Banks", Title = "Product Designer" });
            Add(new Employee { Name = "Matt Robinson", Title = "Software Developer" });
            Add(new Employee { Name = "Brian Mouton", Title = "Software Developer" });
        }

        public IEnumerable<Employee> GetAll()
        {
            return _employees;
        }

        public IEnumerable<Employee> Search(string identifier)
        {
            if (identifier != null)
            {
                List<Employee> result;
                result = _employees.Where(e => (e.Name).Contains(identifier) || (e.Title).Contains(identifier)).ToList();
                return result;
            }
            return null;
        }

        public void Add(Employee employee)
        {
            _employees.Add(employee);
        }
        
    }
}
