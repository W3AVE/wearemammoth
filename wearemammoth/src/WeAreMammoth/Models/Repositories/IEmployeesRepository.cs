﻿using System.Collections.Generic;

namespace WeAreMammoth.Models.Repositories
{
    public interface IEmployeesRepository
    {
        IEnumerable<Employee> GetAll();

        IEnumerable<Employee> Search(string identifier);
    }
}
