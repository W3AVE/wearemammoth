﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeAreMammoth.Models
{
    public class Employee
    {
        public string Name { get; set; }
        public string Title { get; set; }
    }
}
